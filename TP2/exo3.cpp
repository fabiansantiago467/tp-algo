#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	for (uint i = 1; i<toSort.size(); i++){
		for (int j = -1; j<1; j++){
			if (toSort[j+i]>toSort[i]){
					toSort.swap(i,j);
					j=-1;
			}
			if (toSort[j+i] < toSort[i]){
				toSort.swap(i,j);
				j= 1;
			}
		}
	}
}






int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
