#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    BinaryTree* left;
    BinaryTree* right;
    int value;

    void initNode(int value)
    {
        this->value = value;
        this->left = NULL;
        this->right = NULL;
    }

	void insertNumber(int value) {
      BinarySearchTree* nouveau=new BinarySearchTree(value);
        nouveau->initNode(value);

        BinarySearchTree* val = this;
        while(val){
            if (val->isLeaf()==true) {
                if (val->value > value) {
                    val->left = nouveau;
                    break;
                }
                else {
                    val->right = nouveau;
                    break;
                }
            }
            else {
                if (val->value > value) {
                    if (val->left == NULL) {
                        val->left = nouveau;
                        break;
                    }
                    val = val->left;
                }
                else {
                    if (val->right == NULL) {
                        val->right = nouveau;
                        break;
                    }
                    val = val->right;
                }
            }
        }


    }

	uint height() const	{
        if (this->isLeaf()==true)
        {
            return 1;
        }
        else
        {
            uint hauteur_d=0;
            uint hauteur_g=0;

            if(this->left)
            {
                hauteur_g=(this->left)->height();
            }
            if(this->right)
            {
                hauteur_d=(this->right)->height();
            }

            if(hauteur_g>hauteur_d)
            {
                return(hauteur_g+1);
            }
            else
            {
                return(hauteur_d+1);
            }
        }

    }

	uint nodesCount() const {
        int c=0;
        if (this->left != NULL){
            c++;
        }
        if (this->right != NULL){
            c++;
            return c+1;
        }
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        return 1;
    }

	bool isLeaf() const {
        if (this->left != NULL && this->right != NULL){
            return true;
        }
        // return True if the node is a leaf (it has no children)
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
