#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	int i = heapSize;
	this->get(i) = value;
	while(i>0 && this->get(i)>this->get((i-1)/2)){
		swap(i,(i-1)/2);
		i=(i-1)/2;
	}
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	int i_max = nodeIndex;
    if((this->rightChild(nodeIndex))<heapSize &&
            this->get(this->rightChild(nodeIndex))>this->get(i_max))
    {
        i_max=this->rightChild(nodeIndex);
    }
    if((this->leftChild(nodeIndex))<heapSize &&
            this->get(this->leftChild(nodeIndex))>this->get(i_max))
    {
        i_max=this->leftChild(nodeIndex);
    }
    if(i_max!=nodeIndex)
    {
        swap(i_max, nodeIndex);
        heapify(heapSize, i_max);
    }

}

void Heap::buildHeap(Array& numbers)
{
	for (int i = 0; i< numbers.size(); i++){
		insertHeapNode(numbers.size(), numbers[i]);
	}
}

void Heap::heapSort()
{
	for(int i=(this->size())-1; i>=0; i--)
    {
        swap(0, i);
        heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
